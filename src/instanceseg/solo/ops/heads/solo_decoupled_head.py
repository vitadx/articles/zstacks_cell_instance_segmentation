import torch
import torch.nn as nn
import torch.nn.functional as F

from ..blocks.convs import ConvBlock
from .solo_vanilla_head import SOLOCategoryBranch


class SOLODecoupledMaskBranch(nn.Module):

    def __init__(self, in_channels, num_groups, grid_number, num_convs,
                 num_up_conv_blocks):
        super(SOLODecoupledMaskBranch, self).__init__()
        self.grid_number = grid_number
        self.in_channels = in_channels
        self.num_groups = num_groups
        self.num_convs = num_convs
        self.num_up_conv_blocks = num_up_conv_blocks
        self.convs_x = self.get_convs()
        self.convs_y = self.get_convs()
        self.up_convs_x = self.get_up_convs()
        self.up_convs_y = self.get_up_convs()

        self.last_conv_x = nn.Conv2d(in_channels=in_channels,
                                     out_channels=grid_number,
                                     kernel_size=1,
                                     stride=1,
                                     padding=0,
                                     bias=True)
        self.last_conv_y = nn.Conv2d(in_channels=in_channels,
                                     out_channels=grid_number,
                                     kernel_size=1,
                                     stride=1,
                                     padding=0,
                                     bias=True)

    def get_convs(self):
        convs = nn.ModuleList(
            [ConvBlock(conv_params={'name': 'coord_conv',
                                    'in_channels': self.in_channels,
                                    'out_channels': self.in_channels,
                                    'kernel_size': 3,
                                    'stride': 1,
                                    'padding': 1,
                                    'dilation': 1,
                                    'groups': 1,
                                    'bias': True,
                                    'boundaries': [-1, 1]},
                       norm_params={'name': 'GN',
                                    'num_groups': self.num_groups,
                                    'num_channels': self.in_channels})] +
            [ConvBlock(conv_params={'name': 'conv2d',
                                    'in_channels': self.in_channels,
                                    'out_channels': self.in_channels,
                                    'kernel_size': 3,
                                    'stride': 1,
                                    'padding': 1,
                                    'dilation': 1,
                                    'groups': 1,
                                    'bias': True},
                       norm_params={'name': 'GN',
                                    'num_groups': self.num_groups,
                                    'num_channels': self.in_channels})
             for _ in range(self.num_convs-1)])
        return convs

    def get_up_convs(self):
        up_convs = nn.ModuleList(
            [ConvBlock(conv_params={'name': 'conv2d',
                                    'in_channels': self.in_channels,
                                    'out_channels': self.in_channels,
                                    'kernel_size': 3,
                                    'stride': 1,
                                    'padding': 1,
                                    'dilation': 1,
                                    'groups': 1,
                                    'bias': True,
                                    'boundaries': [-1, 1]},
                       norm_params={'name': 'GN',
                                    'num_groups': self.num_groups,
                                    'num_channels': self.in_channels})
             for _ in range(self.num_up_conv_blocks)])
        return up_convs

    def combine_branches(self, branch_x, branch_y):
        N, S, h, w = branch_x.shape
        branch_x = branch_x.unsqueeze(1)
        branch_x = branch_x.repeat(1, S, 1, 1, 1)
        branch_y = branch_y.unsqueeze(2)
        branch_y = branch_y.repeat(1, 1, S, 1, 1)
        masks = branch_x * branch_y
        masks = masks.view(N, S**2, h, w)
        return masks

    def forward(self, x):
        branch_x = x
        branch_y = x
        del x
        for conv_x, conv_y in zip(self.convs_x, self.convs_y):
            branch_x = conv_x(branch_x)
            branch_y = conv_y(branch_y)
        for conv_x, conv_y in zip(self.up_convs_x, self.up_convs_y):
            branch_x = F.interpolate(
                branch_x, scale_factor=2, mode='bilinear', align_corners=True)
            branch_x = conv_x(branch_x)
            branch_y = F.interpolate(
                branch_y, scale_factor=2, mode='bilinear', align_corners=True)
            branch_y = conv_y(branch_y)
        branch_x = self.last_conv_x(branch_x)
        branch_y = self.last_conv_y(branch_y)
        return self.combine_branches(branch_x.sigmoid(), branch_y.sigmoid())


class SOLODecoupledHead(nn.Module):

    def __init__(self, in_channels, num_groups, grid_number, num_convs,
                 num_up_conv_blocks, num_categories):
        super(SOLODecoupledHead, self).__init__()
        self.category_branch = SOLOCategoryBranch(
            in_channels=in_channels,
            num_groups=num_groups,
            grid_number=grid_number,
            num_convs=num_convs,
            num_categories=num_categories)
        self.mask_branch = SOLODecoupledMaskBranch(
            in_channels=in_channels,
            num_groups=num_groups,
            grid_number=grid_number,
            num_convs=num_convs,
            num_up_conv_blocks=num_up_conv_blocks)

    def forward(self, x):
        categories = self.category_branch(x)
        masks = self.mask_branch(x)
        return categories, masks


if __name__ == '__main__':
    pass
