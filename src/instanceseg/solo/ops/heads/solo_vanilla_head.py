import torch
import torch.nn as nn
import torch.nn.functional as F

from ..blocks.convs import ConvBlock, BasicResidualBlock


class SOLOCategoryBranch(nn.Module):

    def __init__(self, in_channels, num_groups, grid_number, num_convs,
                 num_categories):
        super(SOLOCategoryBranch, self).__init__()
        self.grid_number = grid_number
        self.num_categories = num_categories

        conv_params={'name': 'conv2d',
                     'in_channels': in_channels,
                     'out_channels': in_channels,
                     'kernel_size': 3,
                     'stride': 1,
                     'padding': 1,
                     'dilation': 1,
                     'groups': 1,
                     'bias': True}

        norm_params={'name': 'GN',
                     'num_groups': num_groups,
                     'num_channels': in_channels}

        self.convs = [
            ConvBlock(conv_params=conv_params, norm_params=norm_params)
            for _ in range(num_convs)]
        self.convs = nn.ModuleList(self.convs)
        self.last_conv = nn.Conv2d(in_channels=in_channels,
                                   out_channels=num_categories,
                                   kernel_size=1,
                                   stride=1)

    def forward(self, x):
        x = F.interpolate(
            input=x, size=self.grid_number, mode='bilinear', align_corners=True)
        for conv in self.convs:
            x = conv(x)
        x = self.last_conv(x)
        return x


class SOLOMaskBranch(nn.Module):

    def __init__(self, in_channels, num_groups, grid_number, num_convs,
                 num_up_conv_blocks):
        super(SOLOMaskBranch, self).__init__()
        self.grid_number = grid_number
        self.num_up_conv_blocks = num_up_conv_blocks

        self.convs = [ConvBlock(conv_params={'name': 'coord_conv',
                                             'in_channels': in_channels,
                                             'out_channels': in_channels,
                                             'kernel_size': 3,
                                             'stride': 1,
                                             'padding': 1,
                                             'dilation': 1,
                                             'groups': 1,
                                             'bias': True,
                                             'boundaries': [-1, 1]},
                                norm_params={'name': 'GN',
                                             'num_groups': num_groups,
                                             'num_channels': in_channels})]
        self.convs += [ConvBlock(conv_params={'name': 'conv2d',
                                              'in_channels': in_channels,
                                              'out_channels': in_channels,
                                              'kernel_size': 3,
                                              'stride': 1,
                                              'padding': 1,
                                              'dilation': 1,
                                              'groups': 1,
                                              'bias': True},
                                 norm_params={'name': 'GN',
                                              'num_groups': num_groups,
                                              'num_channels': in_channels})
                       for _ in range(num_convs - 1)]
        self.convs = nn.ModuleList(self.convs)

        self.up_convs = [ConvBlock(conv_params={'name': 'conv2d',
                                                'in_channels': in_channels,
                                                'out_channels': in_channels,
                                                'kernel_size': 3,
                                                'stride': 1,
                                                'padding': 1,
                                                'dilation': 1,
                                                'groups': 1,
                                                'bias': True,
                                                'boundaries': [-1, 1]},
                                   norm_params={'name': 'GN',
                                                'num_groups': num_groups,
                                                'num_channels': in_channels})
                         for _ in range(num_up_conv_blocks)]
        self.up_convs = nn.ModuleList(self.up_convs)

        self.last_conv = nn.Conv2d(in_channels=in_channels,
                                   out_channels=int(grid_number**2),
                                   kernel_size=1,
                                   stride=1,
                                   padding=0,
                                   bias=True)

    def forward(self, x):
        for conv in self.convs:
            x = conv(x)
        for up_conv in self.up_convs:
            x = F.interpolate(
                x, scale_factor=2, mode='bilinear', align_corners=True)
            x = up_conv(x)
        x = self.last_conv(x)
        x = nn.Sigmoid()(x)
        return x


class SOLOVanillaHead(nn.Module):

    def __init__(self, in_channels, num_groups, grid_number, num_convs,
                 num_up_conv_blocks, num_categories):
        super(SOLOVanillaHead, self).__init__()

        self.category_branch = SOLOCategoryBranch(in_channels=in_channels,
                                                  num_groups=num_groups,
                                                  grid_number=grid_number,
                                                  num_convs=num_convs,
                                                  num_categories=num_categories)
        self.mask_branch = SOLOMaskBranch(in_channels=in_channels,
                                          num_groups=num_groups,
                                          grid_number=grid_number,
                                          num_convs=num_convs,
                                          num_up_conv_blocks=num_up_conv_blocks)

    def forward(self, x):
        categories = self.category_branch(x)
        masks = self.mask_branch(x)
        return categories, masks


if __name__ == '__main__':
    pass
